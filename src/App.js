import React from "react"
import "./App.css"
import {Table} from "./Table"
import {BrowserRouter, Route} from "react-router-dom"
import {Article} from "./Article"


function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Route exact path="/" component={Table}/>
                <Route path="/:itemID" component={Article}/>
            </BrowserRouter>
        </div>
    )
}

export default App
