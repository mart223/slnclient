import React from "react"
import {TableItem} from "./TableItem"
import {Query} from "react-apollo"
import gql from "graphql-tag"


const FEED_QUERY = gql`
  query {
  newsList(skip: 0, limit: 200) {
    rows {
      id
      title
      img
    }
  }
}
`

export const Table = () => [
    <div className={"TableTitle"} key={"Title"}>
        <h2> NEWS READER </h2>
    </div>,
    <div className={"Table"}>
        <Query query={FEED_QUERY} key={"FeedQuery"}>
            {({loading, error, data}) => {
                if (loading) {
                    return <p> Loading... </p>
                }
                if (error) {
                    return <p> Error </p>
                }
                return data.newsList.rows.map((item, index) => <TableItem item={item} index={index} key={item.id}/>)
            }}
        </Query>
    </div>
]










