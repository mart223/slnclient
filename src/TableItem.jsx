import React, {useCallback} from "react"
import classNames from "classnames"
import { useHistory } from "react-router-dom";


export const TableItem = ({item, index}) => {
    
    const history = useHistory();
    const openItem = useCallback(() => history.push("/" + item.id), [item, history])
    
    return <div className={classNames("Table-item", index % 3 !== 0 && "halfWidth")} onClick={openItem}>
        <img src={item.img || "logo192.png"}
             alt={"Article"}/>
        <h3> {item.title} </h3>
    </div>
    
}










