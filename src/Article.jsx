import React, {useCallback} from "react"
import gql from "graphql-tag"
import {useQuery} from "react-apollo"
import {useHistory} from "react-router-dom"


const ITEM_QUERY = gql`
  query($id: ID!) {
  newsItem(id: $id) {
    id
    title
    content
    url
    img
  }
}
`

export const Article = props => {
    
    const history = useHistory()
    const closeItem = useCallback((event) => history.push("/"), [history])
    
    const itemID = props.match.params.itemID
    const {loading, error, data} = useQuery(ITEM_QUERY, {variables: {id: itemID}})
    
    const header = <div className={"TableTitle"} key={"Title"}>
        <div className={"BackToMain"} onClick={closeItem}><h2> &#8592; </h2></div>
        <h2> NEWS READER </h2>
    </div>
    
    if (loading) {
        return [header, <p key={"Message"}> Loading... </p>]
    }
    
    if (error) {
        return [header, <p key={"Message"}> Error </p>]
    }
    
    if (!data || !data.newsItem) {
        return [header, <p key={"Message"}> No such article </p>]
    }
    
    const item = data.newsItem
    
    return [
        header,
        <div className={"ArticleView"} key={"ArticleContainer"}>
            <h3> {item.title} </h3>
            <img src={item.img || "logo192.png"} alt={"Article"}/>
            <p> {item.content} </p>
            <a href={item.url}> Read more </a>
        </div>
    ]
    
}










